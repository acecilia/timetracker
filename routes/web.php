<?php

use Illuminate\Support\Facades\Route;
use App\Models\Absent;
use App\Models\Employee;
use App\Models\Record;
use App\Models\SecureCode;
use App\Models\Vacation;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', function () {
    return view('home');
});

// Worker Detail
Route::get('/worker-detail/{id}', function($id){
    return view(
        'employeeDetail',
        [
            'id' => $id
        ]);
});


// Axios BBDD - Consultas
Route::post('/employee-info/{id}', function($id){
    return Employee::where('id', $id)->get()[0];
});

Route::post('/securecode/{id_employee}', function($idEmployee){
    return SecureCode::employeeCode($idEmployee);
});

// Axios BBDD - Inserts

Route::post('/securecode/insertRecord/{id_employee}/{status}', function($idEmployee, $status){
    return Record::insert($idEmployee, $status);
});

Route::post('/insertAbsent/{id_employee}/{reason}/{date}', function($idEmployee, $reason, $date){
    return Absent::insert($idEmployee, $reason, $date);
});

Route::post('/insertVacation/{id_employee}/{date}', function($idEmployee, $date){
    return Vacation::insert($idEmployee, $date);
});

// Axios BBDD - Update

Route::post('/updateStatus/{id_employee}/{status}', function($idEmployee, $status){
    return Employee::updateStatus($idEmployee, $status);
});