module.exports = {
    purge: {
        content: ["./resources/**/*.blade.php", "./resources/**/*.vue", "./resources/**/*.js", "./resources/**/*.scss"],
        options: {
            whitelist: [
                'w-1/2',
                'lg:w-1/2',
                'w-1/3',
                'lg:w-1/3',
                'w-1/4',
                'lg:w-1/4',
                'lg:p-1',
                'lg:p-2',
                'lg:p-3',
                'lg:p-4',
                'lg:p-5',
                'lg:px-1',
                'lg:px-2',
                'lg:px-3',
                'lg:px-4',
                'lg:px-5',
                'lg:px-6',
                'lg:py-0'
            ]
        }
    },
    theme: {
        extend: {}
    },
    variants: {},
    plugins: []
};
