<div id="navbar" class="w-40">
    <div class="block p-4">
        @php
            $menu = \App\Models\Menu::all();
        @endphp
        @foreach ($menu as $item)
        <navbar-button-component
            name='{{ $item->name }}'
            url='{{ $item->url }}'
            icon='{{ $item->icon }}'
        ></navbar-button-component>
        @endforeach
    </div>
</div>