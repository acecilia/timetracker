<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Time Tracker</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        {{-- TailwindCSS --}}
        <script src="https://cdn.tailwindcss.com"></script>
        {{-- Font Awesome --}}
        <script src="https://kit.fontawesome.com/4626920aa3.js" crossorigin="anonymous"></script>
        {{-- CSS --}}
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <div id="app" class="flex">
            @include('layout.navbar')