@include('layout.head')
<div class="mx-auto my-4">
    <div class="grid">
        <div>
            <h2 class="text-center uppercase font-bold text-sm">Trabajadores</h2>
            <div class="grid action-pills w-full mx-auto">
                @php
                    $employees = \App\Models\Employee::actives();
                @endphp
                @foreach ($employees as $employee)
                    <employeecard-component 
                        id="{{ $employee->id }}"
                        status="{{ $employee->status }}" 
                        image="{{ $employee->image }}" 
                        name="{{ $employee->name .' '. $employee->surname }}">
                    </employeecard-component>
                @endforeach
            </div>
        </div>
    </div>
</div>
@include('layout.footer')