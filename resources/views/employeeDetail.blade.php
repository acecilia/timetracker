@include('layout.head')
<div class="grid grid-cols-3">
    <div>
        <div class="flex action-pills box-shadow h-28 w-60">
            <enter-component
                id="{{ $id }}">
            </enter-component>
            <exit-component
                id="{{ $id }}">
            </exit-component>
        </div>
        <div>
            @php
                $employee = \App\Models\Employee::where('id', $id)->get()[0];
            @endphp
            <employeedetail-component 
                id="{{ $employee->id }}">
            </employeedetail-component>
        </div>
    </div>
</div>
@include('layout.footer')