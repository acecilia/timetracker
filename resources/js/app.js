window.Vue = require('vue').default;

Vue.component('clock-component', require('./components/ClockComponent.vue').default);
Vue.component('enter-component', require('./components/EnterComponent.vue').default);
Vue.component('exit-component', require('./components/ExitComponent.vue').default);
Vue.component('employeecard-component', require('./components/EmployeeCardComponent.vue').default);
Vue.component('employeedetail-component', require('./components/EmployeeDetailComponent.vue').default);
Vue.component('securecode-component', require('./components/SecureCodeComponent.vue').default);
Vue.component('absent-component', require('./components/AbsentComponent.vue').default);
Vue.component('absentModal-component', require('./components/AbsentModalComponent.vue').default);
Vue.component('vacations-component', require('./components/VacationsComponent.vue').default);
Vue.component('vacationsModal-component', require('./components/VacationsModalComponent.vue').default);
Vue.component('records-component', require('./components/RecordsComponent.vue').default);
Vue.component('recordsModal-component', require('./components/RecordsModalComponent.vue').default);

// New
Vue.component('navbar-button-component', require('./components/NavbarComponent.vue').default);


const app = new Vue({
    el: '#app',
});
