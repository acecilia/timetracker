<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * Recover all the actives employees
     * 
     */
    public static function actives()
    {
        $employees = Employee::where('active',true)->get();
        foreach ($employees as $key => $employee) {
            if(!Record::checkRecord($employee->id)){
                Absent::checkAbsences($employee->id);
            }
        }

        return $employees;
    }

    public static function updateStatus($idEmployee, $status)
    {
        return DB::table('employees')
            ->where('id', $idEmployee)
            ->update(['status' => $status]);
    }
}
