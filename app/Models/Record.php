<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Record extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'records';

    /**
     * Insert employee record
     * 
     */
    public static function insert($idEmployee, $status)
    {
        return DB::table('records')->insert(
            array(
                'id_employee' => $idEmployee,
                'status' => $status,
                'date' => Carbon::now()->toDateTimeString()
            )
        );
    }

    public static function checkRecord($idEmployee)
    {
        $today = Carbon::today()->toDateString();

        $records = DB::table('records')
            ->where('id_employee', $idEmployee)
            ->where('date', 'like', $today.'%')
            ->get();

        if(sizeof($records) > 0){
            return true;
        }else{
            return false;
        }
    }
}
