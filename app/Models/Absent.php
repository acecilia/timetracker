<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Absent extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'absences';

    public static function insert($idEmployee, $reason, $date)
    {
        return DB::table('absences')
            ->insert([
                'id_employee' => $idEmployee,
                'reason' => $reason,
                'date' => $date,
                'status' => 'absent' 
            ]);
    }

    private static function updateStatus($idEmployee, $status)
    {
        $employee = DB::table('employees')
            ->where('id', $idEmployee)
            ->get()[0];

        if($employee->status !== $status){
            DB::table('employees')
                ->where('id', $idEmployee)
                ->update(['status' => $status]);                
        }
    }

    public static function checkAbsences($idEmployee)
    {
        $today = Carbon::today()->toDateString();
        $employee = DB::table('absences')
            ->where('id_employee', $idEmployee)
            ->where('date', $today)
            ->get();

        if(sizeof($employee) > 0){
            $id = ($employee[0]->id_employee);
            $status = ($employee[0]->status);
            Absent::updateStatus($id, $status);
        }

    }
}
