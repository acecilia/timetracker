<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vacation extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'absences';

    public static function insert($idEmployee, $date)
    {
        return DB::table('absences')
            ->insert([
                'id_employee' => $idEmployee,
                'date' => $date,
                'status' => 'vacations',
                'reason' => 'permission'
            ]);
    }

    private static function updateStatus($idEmployee, $status)
    {
        $employee = DB::table('employees')
            ->where('id', $idEmployee)
            ->get()[0];

        if($employee->status !== $status){
            DB::table('employees')
                ->where('id', $idEmployee)
                ->update(['status' => $status]);                
        }
    }

    public static function checkVacations($idEmployee)
    {
        $today = Carbon::today()->toDateString();
        $employee = DB::table('vacations')
            ->where('id_employee', $idEmployee)
            ->where('date', $today)
            ->get();

        if(sizeof($employee) > 0){
            $id = ($employee[0]->id);
            $status = 'vacations';
            Absent::updateStatus($id, $status);
        }

    }
}
