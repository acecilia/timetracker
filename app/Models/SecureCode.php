<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SecureCode extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'secure_codes';

    /**
     * Recover the employee code
     * 
     */
    public static function employeeCode($idEmployee)
    {
        return SecureCode::where('id_employee', $idEmployee)->get();
    }
}
