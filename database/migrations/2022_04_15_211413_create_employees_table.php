<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('dni')->unique();
            $table->string('mail')->unique();
            $table->string('phone')->unique();
            $table->string('ocupation')->nullable()->default(null);
            $table->string('image')->default('trabajador.png');
            $table->enum('status', ['inside', 'outside', 'absent', 'vacations'])->default('outside'); 
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
