<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('employees')->delete();
 
        Employee::insert(array(
            'name' => 'Adrian',
            'surname' => 'Cecilia Ogalla',
            'dni' => '00000000A',
            'mail' => 'acecilia@rica.design',
            'phone' => '999999999',
            'ocupation' => 'Developer'
        ));

        Employee::insert(array(
            'name' => 'John',
            'surname' => 'Doe Lorem',
            'dni' => '00000000B',
            'mail' => 'jdoe@rica.design',
            'phone' => '000000000',
            'ocupation' => 'Developer'
        ));
    }
 
}
