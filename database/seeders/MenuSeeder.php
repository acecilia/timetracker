<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder {
 
    public function run()
    {
        DB::table('menus')->delete();
 
        Menu::insert(array(
            'name' => 'Inicio',
            'url' => '/',
            'icon' => 'fa-solid fa-calendar-days',
        ));

        Menu::insert(array(
            'name' => 'Actividad',
            'url' => '/actividad',
            'icon' => 'fa-solid fa-chart-column',
        ));
    }
 
}
