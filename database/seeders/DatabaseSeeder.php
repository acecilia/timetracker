<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeder Started!');

        $this->call([
            EmployeesTableSeeder::class,
            SecureCodesSeeder::class,
            MenuSeeder::class
        ]);
        
        $this->command->info('Seeder Completed!');
    }
}
