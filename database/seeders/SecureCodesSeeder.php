<?php

namespace Database\Seeders;

use App\Models\SecureCode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SecureCodesSeeder extends Seeder {
 
    public function run()
    {
        DB::table('secure_codes')->delete();
 
        SecureCode::insert(array(
            'id_employee' => 1,
            'code' => 1111
        ));

        SecureCode::insert(array(
            'id_employee' => 2,
            'code' => 2222
        ));
    }
 
}
